@base <http://voparis-ns.obspm.fr/rdf/epn/2.0/spatial-frame-type>.
@prefix : <#>.

@prefix dc: <http://purl.org/dc/terms/> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix foaf: <http://xmlns.com/foaf/0.1/>.
@prefix ivoasem: <http://www.ivoa.net/rdf/ivoasem#>.
@prefix skos: <http://www.w3.org/2004/02/skos/core#>.
@prefix daci: <http://purl.org/datacite/v4.4/>.
@prefix vann: <http://purl.org/vocab/vann/> .

<> a owl:Ontology;
    owl:versionIRI <http://voparis-ns.obspm.fr/rdf/epn/2.0/spatial-frame-type/2024-05-07> ;
    dc:created "2024-05-07";
    vann:preferredNamespacePrefix "epnsftyp";
    dc:creator [ foaf:name "Erard, S." ],
    [ foaf:name "Cecconi, B." ];
    dc:license <http://creativecommons.org/publicdomain/zero/1.0/>;
    rdfs:label "EPNcore Spatial Frame Type"@en;
    dc:title "EPNcore Spatial Frame Type"@en;
    dc:description """Provides the "flavor" of the coordinate system, which defines the
nature of the spatial coordinates (c1,c2,c3) in the EPNCore table and queries,
and the way they are defined. A value is always required (use "none" if not
applicable, although "body" is found in older services and may be OK). This may
be different from the coordinate system associated to / included in the data
themselves. The reference frame itself is defined by the
spatial_coordinate_description parameter, and the frame center can be specified
using the spatial_origin parameter in case of ambiguity.""";
    ivoasem:vocflavour "SKOS".

dc:created a owl:AnnotationProperty.
dc:creator a owl:AnnotationProperty.
dc:title a owl:AnnotationProperty.
dc:description a owl:AnnotationProperty.
owl:versionIRI a owl:AnnotationProperty.
vann:preferredNamespacePrefix a owl:AnnotationProperty.
dc:license a owl:AnnotationProperty.

<> owl:priorVersion <http://voparis-ns.obspm.fr/rdf/epn/2.0/spatial-frame-type/2023-11-11> .
owl:priorVersion a owl:AnnotationProperty.

<#body> a skos:Concept;
  skos:prefLabel "Body-fixed Frame";
  skos:definition "2D angles in body-fixed frame: longitude c1 and latitude c2 + possibly altitude as c3. A planetocentric system with eastward longitudes in the range (0,360)° is required for service interoperability. Current IAU frame is assumed, in particular for definition of the prime meridian. IAU 2009 planetocentric convention applies, in particular eastward longitudes and a north pole located on the north side of the invariant plane of the Solar System for planets and satellites. Parameter c3 is measured above the reference ellipsoid, and can be >0 for interiors. Two other parameters are available to provide other vertical scales if needed (radial_distance and altitude_fromshape). Planetocentric rotating frames are defined as spherical (rather than body).";
  ivoasem:preliminary ":__".

<#cartesian> a skos:Concept;
  skos:prefLabel "Cartesian Frame";
  skos:definition "(x,y,z) as (c1,c2,c3). This includes spatial coordinates given in km.";
  ivoasem:preliminary ":__".

<#celestial> a skos:Concept;
  skos:prefLabel "Celestial Frame";
  skos:definition "2D angles on the sky, i. e., right ascension c1 and declination c2 + possibly heliocentric distance in c3 (in au). Although this is a special case of spherical frame, the order and conventions are different. Right ascension is provided in degrees. ICRS coordinates are assumed by default, but other frames may exist, e.g. HPC (Helio-Projective Cartesian) is centered on the Sun. Such frames are identified from the spatial_coordinate_description parameter. For ground-based observations, Earth distance may be provided in the earth_distance parameter (not in c3).";
  ivoasem:preliminary ":__".

<#cylindrical> a skos:Concept;
  skos:prefLabel "Cylindrical Frame";
  skos:definition "(r, theta, z) as (c1,c2,c3). r = radius in km. theta = azimuth. z = height in km. The angle is provided in degrees.";
  ivoasem:preliminary ":__".

<#healpix> a skos:Concept;
  skos:prefLabel "Healpix Frame";
  skos:definition "usage TBC, will conform to IVOA decision.";
  ivoasem:preliminary ":__".

<#none> a skos:Concept;
  skos:prefLabel "None";
  skos:definition "to be used when no spatial frame is defined for the dataset. This is intended to prevent useless searches when space coordinates are not defined.";
  ivoasem:preliminary ":__".

<#spherical> a skos:Concept;
  skos:prefLabel "Spherical Frame";
  skos:definition "(r, theta, phi) as (c1,c2,c3), as defined in ISO standard 80000-2:2009. r = radius. theta = zenith angle/colatitude/inclination. phi = azimuth (E longitude). Angles are provided in degrees. When related to the sky or tied to a solid body, \"celestial\" (with RA/Dec) or \"body\" (with E longitude/latitude) frames must be used instead.";
  ivoasem:preliminary ":__".

