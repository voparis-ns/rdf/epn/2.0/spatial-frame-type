<?xml version="1.0" encoding="UTF-8"?>
<rdf:RDF xml:lang="en" xmlns="http://voparis-ns.obspm.fr/epn/spatial_frame_type#" xmlns:dc="http://purl.org/dc/terms/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:ivoasem="http://www.ivoa.net/rdf/ivoasem#" xmlns:owl="http://www.w3.org/2002/07/owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#">
    <owl:AnnotationProperty rdf:about="http://purl.org/dc/terms/created"/>
    <owl:AnnotationProperty rdf:about="http://purl.org/dc/terms/creator"/>
    <owl:AnnotationProperty rdf:about="http://purl.org/dc/terms/description"/>
    <owl:AnnotationProperty rdf:about="http://purl.org/dc/terms/title"/>
    <owl:Ontology rdf:about="http://voparis-ns.obspm.fr/epn/spatial_frame_type">
        <dc:created>2023-11-11</dc:created>
        <dc:creator>
            <rdf:Description>
                <foaf:name>Erard, S.</foaf:name>
            </rdf:Description>
        </dc:creator>
        <dc:contributor>
            <rdf:Description>
                <foaf:name>Cecconi, B.</foaf:name>
            </rdf:Description>
        </dc:contributor>
        <dc:description>Provides the "flavor" of the coordinate system, which defines the 
            nature of the spatial coordinates (c1,c2,c3) in the EPNCore table and queries, 
            and the way they are defined. A value is always required (use "none" if not 
            applicable, although "body" is found in older services and may be OK). This may 
            be different from the coordinate system associated to / included in the data 
            themselves. The reference frame itself is defined by the 
            spatial_coordinate_description parameter, and the frame center can be specified 
            using the spatial_origin parameter in case of ambiguity.</dc:description>
        <dc:license rdf:resource="http://creativecommons.org/publicdomain/zero/1.0/"/>
        <dc:title xml:lang="en">EPNcore Spatial Frame Type</dc:title>
        <ivoasem:vocflavour>RDF Class</ivoasem:vocflavour>
        <rdfs:label xml:lang="en">EPNcore Spatial Frame Type</rdfs:label>
    </owl:Ontology>    
    <rdfs:Class rdf:about="http://voparis-ns.obspm.fr/epn/spatial_frame_type#celestial">
        <rdfs:label xml:lang="en">Celestial Frame</rdfs:label>
        <rdfs:comment xml:lang="en">2D angles on the sky, i. e., right ascension c1 and 
            declination c2 + possibly heliocentric distance in c3 (in au); although this 
            is a special case of spherical frame, the order and conventions are different. 
            Right ascension is provided in degrees. ICRS coordinates are assumed by default, 
            but other frames may exist, e.g. HPC (Helio-Projective Cartesian) is centered 
            on the Sun; such frames are identified from the spatial_coordinate_description 
            parameter. For ground-based observations, Earth distance may be provided in 
            the earth_distance parameter (not in c3).</rdfs:comment>
    </rdfs:Class>
    <rdfs:Class rdf:about="http://voparis-ns.obspm.fr/epn/spatial_frame_type#body">
        <rdfs:label xml:lang="en">Body-fixed Frame</rdfs:label>
        <rdfs:comment xml:lang="en">2D angles in body-fixed frame: longitude c1 and 
            latitude c2 + possibly altitude as c3. 

            A planetocentric system with eastward longitudes in the range (0,360)° 
            is required for service interoperability. Current IAU frame is assumed, 
            in particular for definition of the prime meridian. IAU 2009 planetocentric 
            convention applies, in particular eastward longitudes and a north pole 
            located on the north side of the invariant plane of the Solar System for 
            planets and satellites. 
            Parameter c3 is measured above the reference ellipsoid, and can be &lt;0 
            for interiors. Two other parameters are available to provide other vertical 
            scales if needed (radial_distance and altitude_fromshape). 
            
            Planetocentric rotating frames are defined as spherical (rather than 
            body).</rdfs:comment>
    </rdfs:Class>
    <rdfs:Class rdf:about="http://voparis-ns.obspm.fr/epn/spatial_frame_type#cartesian">
        <rdfs:label xml:lang="en">Cartesian Frame</rdfs:label>
        <rdfs:comment xml:lang="en">(x,y,z) as (c1,c2,c3). This includes spatial 
            coordinates given in km.</rdfs:comment>        
    </rdfs:Class>        
    <rdfs:Class rdf:about="http://voparis-ns.obspm.fr/epn/spatial_frame_type#spherical">
        <rdfs:label xml:lang="en">Spherical Frame</rdfs:label>
        <rdfs:comment xml:lang="en">(r, theta, phi) as (c1,c2,c3), as defined in ISO 
            standard 80000-2:2009; r = radius; theta = zenith angle/colatitude/inclination; 
            phi = azimuth (E longitude). Angles are provided in degrees. When related to 
            the sky or tied to a solid body, "celestial" (with RA/Dec) or "body" (with E 
            longitude/latitude) frames must be used instead.</rdfs:comment>        
    </rdfs:Class>        
    <rdfs:Class rdf:about="http://voparis-ns.obspm.fr/epn/spatial_frame_type#cylindrical">
        <rdfs:label xml:lang="en">Cylindrical Frame</rdfs:label>
        <rdfs:comment xml:lang="en">(r, theta, z) as (c1,c2,c3); r = radius in km; theta = 
            azimuth; z = height in km. The angle is provided in degrees.</rdfs:comment>        
    </rdfs:Class>        
    <rdfs:Class rdf:about="http://voparis-ns.obspm.fr/epn/spatial_frame_type#none">
        <rdfs:label xml:lang="en">None</rdfs:label>
        <rdfs:comment xml:lang="en">to be used when no spatial frame is defined for the 
            dataset. This is intended to prevent useless searches when space coordinates 
            are not defined.</rdfs:comment>        
    </rdfs:Class>        
    <rdfs:Class rdf:about="http://voparis-ns.obspm.fr/epn/spatial_frame_type#healpix">
        <rdfs:label xml:lang="en">Healpix Frame</rdfs:label>
        <rdfs:comment xml:lang="en">usage TBC, will conform to IVOA decision.</rdfs:comment>        
    </rdfs:Class>        
</rdf:RDF>